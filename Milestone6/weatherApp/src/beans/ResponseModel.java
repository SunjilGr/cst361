package beans;

/**
 * Response model class with status and message variables
 * @author Aaron and Sunjil
 *
 */
public class ResponseModel {
	
	int status;
	String message;
	
	/**
	 * Non-default constructor for response model initializing status and message.
	 * @author Aaron and Sunjil
	 *
	 */
	public ResponseModel(int status, String message) {
		
		this.status = status;
		this.message = message;
	}
	
	/**
	 * Get status method
	 * @author Aaron and Sunjil
	 *
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * Set status method
	 * @author Aaron and Sunjil
	 *
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * Get message method
	 * @author Aaron and Sunjil
	 *
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Set message method
	 * @author Aaron and Sunjil
	 *
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

}
