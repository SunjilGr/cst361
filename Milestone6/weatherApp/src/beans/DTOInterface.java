package beans;

import java.util.List;

/**
 * This is the DTO interface class creating a "contract" by which dto objects must fulfill.
 * @author Aaron and Sunjil
 *
 */
public interface DTOInterface {

	public List<Weather> getData();
	public void setData(List<Weather> data);
}
