--<ScriptOptions statementTerminator=";"/>

ALTER TABLE "WEATHERDB"."USERS" DROP CONSTRAINT "PRIMARY_KEY";

DROP INDEX "WEATHERDB"."SQL180927181315380";

DROP TABLE "WEATHERDB"."USERS";

CREATE TABLE "WEATHERDB"."USERS" (
		"ID" INTEGER DEFAULT AUTOINCREMENT: start 1 increment 1 NOT NULL GENERATED ALWAYS AS IDENTITY  (START WITH 1 ,INCREMENT BY 1),
		"FIRSTNAME" VARCHAR(25) NOT NULL,
		"LASTNAME" VARCHAR(25) NOT NULL,
		"USERNAME" VARCHAR(25) NOT NULL,
		"PASSWORD" VARCHAR(25) NOT NULL
	);

CREATE UNIQUE INDEX "WEATHERDB"."SQL180927181315380" ON "WEATHERDB"."USERS" ("ID" ASC);

ALTER TABLE "WEATHERDB"."USERS" ADD CONSTRAINT "PRIMARY_KEY" PRIMARY KEY ("ID");

