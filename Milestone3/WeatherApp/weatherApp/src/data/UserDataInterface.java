/**
 * Class: UserDataInterface
 * Objective: This is the data interface class. This is used to access methods inside DAO class. Inversion of control (IOC) is made possible by this class
 * Detail: It contains two method registerUser and login. Both of them are override in DAO class
 * Date: 9/30/2018
 */
package data;

import beans.User;
import javax.ejb.Local;

@Local
public interface UserDataInterface {
	
	/**
	 * Insert user's information into database to register them
	 * @param fName
	 * @param lName
	 * @param uName
	 * @param pass
	 */
	public void registerUser(String fName, String lName, String uName, String pass);
	
	/**
	 * Check username and password to login. Return true if he information matches and false if they don't
	 * @param uName
	 * @param pass
	 * @return
	 */
	public boolean login(String uName, String pass);

}
