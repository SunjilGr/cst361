/**
 * Class: UserDataService
 * Objective: Connect to the database and query table 
 * Detail: This class contains two method registeruser and login. registeruser is used to register user and login is used to login to the website using username and password
 * Date: 9/30/2018
 * Reference: CST-235
 */
package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;


import beans.User;

@Stateless
@Local(UserDataInterface.class)
@LocalBean
public class UserDAO implements UserDataInterface{

	User user= new User();
	
	
	@Override
	/**
	 * This method is used to connect to connect to the database and insert user information into database
	 * It takes firstname, lastname, username, and password as parameter
	 * It does not return anythings
	 */
	public void registerUser(String fName, String lName, String uName, String pass)
	{
		//connection string
		//jdbc:mysql://localhost:3306/weatherdb
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/weatherdb";
		String username = "user";
		String password = "derby";
		PreparedStatement sql = null;
		
		//try block that contains insert query
		try {
			// use prepare statement to execute sql
			conn = DriverManager.getConnection(url, username, password);
			sql = conn.prepareStatement(
					"INSERT INTO WeatherDB.USERS(FIRSTNAME, LASTNAME, USERNAME, PASSWORD) VALUES (?,?,?,?)");
			sql.setString(1, fName);
			sql.setString(2, lName);
			sql.setString(3, uName);
			sql.setString(4, pass);
			sql.executeUpdate();
			System.out.println("**********User successfully added to db**********");
		}
		// execution catching
		catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} 
				//catch block to catch exception
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * This method is used to connection to the database and check user's credentials in order to allow access to the website.
	 * It takes username and password as parameters and return boolean as an ouptut. It returns true if the username and password matches and false if they do not.
	 */
	@Override
	public boolean login(String uName, String pass) {
		// connect to db
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/weatherdb";
		String username = "user";
		String password = "derby";
		// sql statement
		PreparedStatement sql = null;
		ResultSet rs = null;
		try {
			
			conn = DriverManager.getConnection(url, username, password);
			sql = conn.prepareStatement("SELECT * FROM WeatherDB.USERS WHERE USERNAME = ? AND PASSWORD = ?");
			// set data to corresponding attribute
			sql.setString(1, uName);
			sql.setString(2, pass);
			rs = sql.executeQuery();
			int count = 0;

			while (rs.next()) {
				++count;
			}
			// if no users are found, return false
			if (count == 0) {
				System.out.println("No users found");
				return false;
			}
			
			// if user is found, return true
			else if (count == 1) {
				System.out.println("1 user found");
				return true;
			}
			rs.close();
		}
		// exception catching
		catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
}
