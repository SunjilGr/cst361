package business;

import javax.ejb.Local;

@Local
public interface UserBusinessInterface {

	/**
	 *  Insert user's information into database to register them
	 * @param uName
	 * @param pass
	 * @return
	 */
	public boolean login(String uName, String pass);
	
	/**
	 * Check username and password to login. Return true if the information matches and false if they don't
	 * @param fName
	 * @param lName
	 * @param uName
	 * @param pass
	 */
	public void register(String fName, String lName, String uName, String pass);
}
