/**
 * Class: UserBusinessService
 * Objective: Contains business logic required to login and register user
 * Detail: It contains two method register and login which is user to register and login user respectively.
 * Date: 9/30/2018
 * 
 */
package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import data.UserDAO;


@Stateless
@Local(UserBusinessInterface.class)
@Alternative

public class UserBusinessService implements UserBusinessInterface {

	@Inject
	UserDAO userService;
	
	@Override
	/**
	 * This method contains bussiness logic to register user. 
	 * It takes firstname, lastaname, username, and password as parameter 
	 * It is a method with void return type therefore it does not return anything
	 */
	public void register(String fName, String lName, String uName, String pass) {
		userService.registerUser(fName, lName, uName, pass);

	}

	@Override
	/**
	 * This method contains bussiness logic to login user.
	 * It takes username and password as parameter and return true or false based on the result provided by DAO class
	 */
	public boolean login(String uName, String pass){
		if (userService.login(uName, pass) == true)
			return true;
		else
			return false;

	}

}
