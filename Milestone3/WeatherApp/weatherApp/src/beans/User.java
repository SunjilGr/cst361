package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ViewScoped
@ManagedBean

/**
 * 
 * @author sunze
 *
 */
public class User {
	
	//Variables with Data validation constraints 
	
	@NotNull(message="First name is a required field")
	@Size(min=5, max=50, message="First Name must be 5 to 50 character long")
	public String firstName="";
	
	@NotNull(message="Last name is a required field")
	@Size(min=5, max=50, message="Last name must be 5 to 50 character long")
	public String lastName="";
	
	@NotNull(message="Username is a required field")
	@Size(min=5, max=50, message="Username must be 5 to 50 character long")
	public String username="";
	
	@NotNull(message="Password is a required field")
	@Size(min=5, max=50, message="Password must be 5 to 50 character long")
	public String password="";
	
	//Default Constructor
	public User()
	{
		firstName="";
		lastName="";
		username="";
		password="";
	}

	
	//getters and setters
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
