/**
 * Class: UserController
 * Objective: Direct request to correct pages and methods
 * Detail: This class contains two method onSubmitRegsitration and OnSumbit login which is used to register and login user respectively and direct them to correct pages
 * Date: 9/30/2018
 * Reference: CST-235
 */
package controllers;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.UserBusinessInterface;

@ManagedBean
@ViewScoped
public class UserController {
	
	@Inject
	UserBusinessInterface interf;
	
	/**
	 * This method is called when user submit their information on RegistrationForm.xhtml. 
	 * It directs user to the LoginForm if the all the required fields are filled out.
	 * @return
	 * @throws IOException
	 */
	public String onSubmitRegistration() throws IOException
	{
		// get the User Managed Bean
				FacesContext context = FacesContext.getCurrentInstance();
				User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

				// forward to Login form along with the User Managed Bean
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				//Putting user filled out information from the form to the database through bussiness interface
				interf.register(user.getFirstName(), user.getLastName(), user.getUsername(), user.getPassword());
				return "LoginForm.xhtml";
	}
	
	/**
	 * This method is called when user submit login inforamtion using LoginForm.xhtml
	 * It directs user to the HomePage if the username and password matches with information on database
	 * 
	 * @return
	 * @throws IOException
	 */
	public String onSubmitLogin() throws IOException {
		// get the User Managed Bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		// forward to HomePage form along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);

		// if creds are correct, login, else don't
		if (interf.login(user.getUsername(), user.getPassword()) == true) {
			return "HomePage.xhtml";
		} else
			return "LoginForm.xhtml";
	}

	
	public UserBusinessInterface getInterface() {
		return interf;
	}

}
