import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.HttpResponse;

/**
 * Source for code https://www.igorkromin.net/index.php/2017/01/01/a-simple-openweathermap-example-in-java/
 * @author Aaron with help from Sunjil and Professor Mark Reha
 *
 */
public class TestWeatherMap {
	public static final void main(String[] args) throws ClientProtocolException, IOException  {
		//API Key to get information
		String owmApiKey = "d8eefbf03b749f99b7b9eb815b4c5863";
		//Id for Phoenix AZ listed in the URI 5308480
		String cityID = "5308480";
		
		//URL for the api call: http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=d8eefbf03b749f99b7b9eb815b4c5863
		
		//HTTP Client
		HttpClient client = new DefaultHttpClient();
		//Requesting the information with the city id and the api key in the uri
		HttpGet request = new HttpGet("http://api.openweathermap.org/data/2.5/weather?id=" + cityID + "&mode=json&units=imperial&APPID=" + owmApiKey);
		//response from openweathermap
		HttpResponse response = client.execute(request);
		//reading the response content
		BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		//putting the content on the console
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}

	}

}
