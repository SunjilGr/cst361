import java.io.BufferedReader;
import org.json.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.HttpResponse;

/**
 * Source for code https://www.igorkromin.net/index.php/2017/01/01/a-simple-openweathermap-example-in-java/
 * @author Aaron with help from Sunjil and Professor Mark Reha
 *
 */
public class TestWeatherMap {
	public static final void main(String[] args) throws ClientProtocolException, IOException  {
		//API Key to get information
		String owmApiKey = "d8eefbf03b749f99b7b9eb815b4c5863";
		//Id for Phoenix AZ listed in the URI 5308480
		String cityID = "5308480";
		
		//URL for the api call: http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=d8eefbf03b749f99b7b9eb815b4c5863
		
		//HTTP Client
		HttpClient client = new DefaultHttpClient();
		//Requesting the information with the city id and the api key in the uri
		HttpGet request = new HttpGet("http://api.openweathermap.org/data/2.5/weather?id=" + cityID + "&mode=json&units=imperial&APPID=" + owmApiKey);
		//response from openweathermap
		HttpResponse response = client.execute(request);
		//reading the response content
		BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
		String json = "";
		String testLine = "";
		//putting the content into one string
		while ((testLine = rd.readLine()) != null) {
			
			json += testLine;
			
		}
		//Creating the object mapper from Jackson
		ObjectMapper objectMapper = new ObjectMapper();
		
		//reading the json string
		JsonNode rootNode = objectMapper.readTree(json);
		//using the path() method to get the main path and the temp from the json string
		JsonNode tempNode = rootNode.path("main").path("temp");
		//putting the node to a string variable
		String temp = tempNode.toString();
		
		//using the path() method to get the main path and the humidity from the json string
		JsonNode humidityNode = rootNode.path("main").path("humidity");
		//putting the node to a string variable
		String humidity = humidityNode.toString();
		
		//using the path() method to get the main path and the minimum temp from the json string
		JsonNode tempMinNode = rootNode.path("main").path("temp_min");
		//putting the node to a string variable
		String tempMin = tempMinNode.toString();
		
		//using the path() method to get the main path and the maximum temp from the json string
		JsonNode tempMaxNode = rootNode.path("main").path("temp_max");
		//putting the node to a string variable
		String tempMax = tempMaxNode.toString();
		
		//Printing out to the console so we know exactly what we're going to send
		System.out.println(temp + " degrees, " + humidity + "% humidity, " + tempMin + " minimum degrees, " + tempMax + " maximum degrees.");
		
		//post to http://localhost:8080/weatherapp/rest/
		HttpPost post = new HttpPost("http://localhost:8080/weatherapp/rest/");
		
		final String POST_PARAMS = "{\n" + "\"temp\": " + temp + ",\r\n" +
		        "    \"humidity\": " + humidity + ",\r\n" +
		        "    \"tempMin\": " + tempMin + ",\r\n" +
		        "    \"tempMax\": " + tempMax + "" + "\n}";
		    System.out.println(POST_PARAMS);
		    URL obj = new URL("http://localhost:8080/weatherapp/rest/");
		    HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
		    postConnection.setRequestMethod("POST");
		    postConnection.setRequestProperty("weather", "json");
		    postConnection.setRequestProperty("Content-Type", "application/json");
		    postConnection.setDoOutput(true);
		    OutputStream os = postConnection.getOutputStream();
		    os.write(POST_PARAMS.getBytes());
		    os.flush();
		    os.close();

		    int responseCode = postConnection.getResponseCode();
		    System.out.println("POST Response Code :  " + responseCode);
		    System.out.println("POST Response Message : " + postConnection.getResponseMessage());
		    if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
		        BufferedReader in = new BufferedReader(new InputStreamReader(
		            postConnection.getInputStream()));
		        String inputLine;
		        StringBuffer response1 = new StringBuffer();
		        while ((inputLine = in .readLine()) != null) {
		            response1.append(inputLine);
		        } in .close();
		        // print result
		        System.out.println(response1.toString());
		    } else {
		        System.out.println("POST NOT WORKED");
		    }
	}

}
