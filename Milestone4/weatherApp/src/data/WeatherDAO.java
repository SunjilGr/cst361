package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Weather;
import util.DatabaseException;
import util.LoggingInterceptor;

/**
 * The Class WeatherDAO.
 *
 * @author Aaron and Sunjil
 * 
 *         Weather database access object implements weather data interface
 */
@Stateless
@Local(WeatherDataInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class WeatherDAO implements WeatherDataInterface<Weather> {
	
	/** The data. */
	//New weather model of data and boolean result
	Weather data= new Weather();
	
	/** The result. */
	boolean result;
	
	/** The logger. */
	Logger logger= LoggerFactory.getLogger(WeatherDAO.class);

	/** The info. */
	String info ="Database Error. Please try again later. Thank you!";

	
	/**
	 * Creates the weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Create method for weather data
	 * @param weather the weather
	 * @return true, if successful
	 */
	@Override
	public boolean createWeatherData(Weather weather)
	{
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   
			//connection string
			Connection conn = null;
			String url = "jdbc:derby:C:\\\\Users\\\\awros\\\\WeatherDB";
			String username = "user";
			String password = "derby";
			PreparedStatement sql = null;
			
			//try block that contains insert query
			try {
				// use prepare statement to execute sql
				conn = DriverManager.getConnection(url, username, password);
				sql = conn.prepareStatement(
						"INSERT INTO WEATHER.WEATHER(TEMP, TEMPMIN, TEMPMAX, HUMIDITY,DATEANDTIME) VALUES (?,?,?,?,?)");
				sql.setDouble(1, weather.getTemperature());
				sql.setDouble(2, weather.getMinTemp());
				sql.setDouble(3, weather.getMaxTemp());
				sql.setDouble(4, weather.getHumidity());
				//sql.setString(5,"1");
				sql.setString(5, dtf.format(now));
				
				sql.executeUpdate();
				sql.close();
			}
			catch (SQLException e) {
				logger.error(e.toString());
				throw new DatabaseException(e);
			}
			return result;
					
	}
	
	/* (non-Javadoc)
	 * @see data.WeatherDataInterface#findAll()
	 */
	@Override
	/**
	 * SELECT all products from products table
	 */
	public List<Weather> findAll() {
		//connection string
		Connection conn = null;
		String url = "jdbc:derby:C:\\Users\\awros\\WeatherDB";
		String username = "user";
		String password = "derby";
		//PreparedStatement sql1 = null;
		// sql statement
		 String sql1 = "SELECT * FROM WEATHER.WEATHER";
		List<Weather> weatherList = new ArrayList<Weather>();

		try {
			// connection string
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql1);
			// set data to corresponding attribute
			while (rs.next()) {
				
				data.setWID(rs.getInt("WID"));
				data.setTemperature(rs.getDouble("TEMP"));
				data.setMinTemp(rs.getDouble("TEMPMIN"));
				data.setMaxTemp(rs.getDouble("TEMPMAX"));
				data.setHumidity(rs.getDouble("HUMIDITY"));
				data.setDateAndTime(rs.getString("DATEANDTIME"));
			   
				weatherList.add(new Weather(rs.getInt("WID"), rs.getDouble("TEMP"), rs.getDouble("TEMPMIN"), rs.getDouble("TEMPMAX"), rs.getDouble("HUMIDITY"), rs.getString("DATEANDTIME")));
			}
			// close result set
			rs.close();
			stmt.close();
		}
		// exception catching
		catch (SQLException e) {
			logger.error(e.toString());
			throw new DatabaseException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(e.toString());
					throw new DatabaseException(e);
				}
			}
		}
		// return products list
		return weatherList;
	}

	

	
	

}
