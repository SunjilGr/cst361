/**
 * Class: UserDataService
 * Objective: Connect to the database and query table 
 * Detail: This class contains two method registeruser and login. registeruser is used to register user and login is used to login to the website using username and password
 * Date: 9/30/2018
 * Reference: CST-235
 */
package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;
import util.DatabaseException;
import util.LoggingInterceptor;

/**
 * The Class UserDAO.
 *
 * @author Aaron and Sunjil
 * 
 *         User database access object class that implements the user data
 *         interface
 */
@Stateless
@Local(UserDataInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class UserDAO implements UserDataInterface<User> {
	
	 /** The logger. */
 	Logger logger= LoggerFactory.getLogger(UserDAO.class);

	/** The info. */
	String info ="Database Error. Please try again later. Thank you!";
	
	/**
	 * This method is used to connect to connect to the database and insert user
	 * information into database It takes firstname, lastname, username, and
	 * password as parameter It does not return anythings.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	@Override
	public boolean registerUser(User user) {
		// connection string
		Connection conn = null;
		String url = "jdbc:derby:C:\\Users\\awros\\WeatherDB";
		String username = "user";
		String password = "derby";
		PreparedStatement sql = null;
		

		// try block that contains insert query
		try {
			// use prepare statement to execute sql
			conn = DriverManager.getConnection(url, username, password);
			sql = conn.prepareStatement(
					"INSERT INTO WEATHER.USERS(FIRSTNAME, LASTNAME, USERNAME, PASSWORD) VALUES (?,?,?,?)");
			sql.setString(1, user.firstName);
			sql.setString(2, user.lastName);
			sql.setString(3, user.username);
			sql.setString(4, user.password);
			sql.executeUpdate();
			System.out.println("**********User successfully added to db**********");
			sql.close();
			return true;
		}
		// execution catching
		catch (SQLException e) {
		    logger.error(e.toString());
		    System.out.println(e);
			throw new DatabaseException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					  logger.error(e.toString());
						throw new DatabaseException(e);
				}
			}
		}

	}

	/**
	 * This method is used to connection to the database and check user's
	 * credentials in order to allow access to the website. It takes username and
	 * password as parameters and return boolean as an ouptut. It returns true if
	 * the username and password matches and false if they do not.
	 *
	 * @param user the user
	 * @return true, if successful
	 */

	@Override
	public boolean login(User user) {
		// connect to db
		Connection conn = null;
		String url = "jdbc:derby:C:\\Users\\awros\\WeatherDB";
		String username = "user";
		String password = "derby";
		// sql statement
		PreparedStatement sql = null;
		ResultSet rs = null;
		try {

			conn = DriverManager.getConnection(url, username, password);
			sql = conn.prepareStatement("SELECT * FROM WEATHER.USERS WHERE USERNAME = ? AND PASSWORD = ?");
			// set data to corresponding attribute
			sql.setString(1, user.username);
			sql.setString(2, user.password);
			rs = sql.executeQuery();
			int count = 0;

			while (rs.next()) {
				++count;
			}
			// if no users are found, return false
			if (count == 0) {
				System.out.println("No users found");
				return false;
			}

			// if user is found, return true
			else if (count == 1) {
				System.out.println("1 user found");
				return true;
			}
			sql.close();

			rs.close();
		}
		// exception catching
		catch(SQLException e){
			  logger.error(e.toString());
				throw new DatabaseException(e);
	 }
		finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					  logger.error(e.toString());
						throw new DatabaseException(e);
				}
			}
		}
		return false;
	}

}
