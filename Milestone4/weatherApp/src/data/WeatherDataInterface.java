package data;

import java.util.List;

/**
 * The Interface WeatherDataInterface.
 *
 * @author Aaron and Sunjil
 * 
 *         Interface for weather data
 * @param <T> the generic type
 */
public interface WeatherDataInterface <T> {
	
	/**
	 * Creates the weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         create method for weather data
	 * @param t the t
	 * @return true, if successful
	 */
	public boolean createWeatherData(T t);

	/**
	 * Find all.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Find all method
	 * @return the list
	 */
	public List<T> findAll();

}
