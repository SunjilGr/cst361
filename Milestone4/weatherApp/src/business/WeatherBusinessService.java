package business;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Weather;

import data.WeatherDataInterface;
import util.LoggingInterceptor;


/**
 * The Class WeatherBusinessService.
 *
 * @author Aaron and Sunjil
 * 
 *         Weather business service class that implements the weather business interface and injects the weather data interface for weather data service
 */
@Stateless
@Local(WeatherBusinessInterface.class)
@Interceptors(LoggingInterceptor.class)
public class WeatherBusinessService implements WeatherBusinessInterface{
	
	/** The service. */
	@Inject
	WeatherDataInterface<Weather> service ;
	
	/** The logger. */
	Logger logger = LoggerFactory.getLogger(WeatherBusinessService.class);

	/**
	 * Insert weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Methods for inserting weather data
	 * @param weather the weather
	 */
	public void insertWeatherData(Weather weather)
	{
		//Creating weather data using given weather
		logger.info("Entering insertWeatherData method in WeatherBusinessService class");
		service.createWeatherData(weather);
		logger.info("Existing insertWeatherData method in WeatherBusinessService class");
	}
	
	/**
	 * Gets the weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Methods for getting weather data
	 * @return the weather data
	 */
	public List<Weather> getWeatherData()
	{
		//Getting all weather

		logger.info("Entering getWeatherData method in WeatherBusinessService class");
		logger.info("Existing getWeatherData method in WeatherBusinessService class");
		return service.findAll();
	}
	
	

}
