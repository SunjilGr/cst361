package business;



import javax.ejb.Local;

import beans.User;
import util.UserNotFoundException;


/**
 * The Interface UserBusinessInterface.
 *
 * @author Aaron and Sunjil
 * 
 *         Interface for the user business service
 */
@Local
public interface UserBusinessInterface {

	/**
	 *  Insert user's information into database to register them.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws UserNotFoundException the user not found exception
	 */
	public boolean login(User user)throws UserNotFoundException;
	
	/**
	 * Check username and password to login. Return true if the information matches and false if they don't
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean register(User user);
	
}
