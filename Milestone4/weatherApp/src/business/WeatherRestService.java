package business;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.DTOFactoryClass;
import beans.DTOInterface;

import beans.Weather;

import data.WeatherDataInterface;
import util.LoggingInterceptor;

/**
 * The Class WeatherRestService.
 *
 * @author Aaron and Sunjil
 * 
 *         Weather rest service class that injects the weather data interface
 *         (DAO) and initializes the logger
 */
@RequestScoped
@Path("/weather")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
@Interceptors(LoggingInterceptor.class)
public class WeatherRestService {

	/** The dao. */
	@Inject
	WeatherDataInterface<Weather> dao;

	/** The logger. */
	Logger logger = LoggerFactory.getLogger(WeatherRestService.class);

	/**
	 * Save temperature.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         method for saving the temperature and returning a response
	 * @param weather the weather
	 * @return the DTO interface
	 */
	@POST
	@Path("/save")
	@Consumes("application/json")
	@Produces("application/json")
	public DTOInterface saveTemperature(Weather weather) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   
		List<Weather> weatherList = new ArrayList<Weather>();
		weatherList.add(new Weather(0, weather.getTemperature(), weather.getMinTemp(), weather.getMaxTemp(), weather.getHumidity(), dtf.format(now)));
		DTOFactoryClass dtoFactory = new DTOFactoryClass();
		//Creating the response
		DTOInterface response = dtoFactory.DTOFactory(0, "", weatherList);
		// create the weather data
		dao.createWeatherData(weather);
		
		return response;
	}

	/**
	 * Gets the all data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getting all data for charts. Takes the DTO interface
	 * @return the all data
	 */
	@GET
	@Path("/getChartData")
	@Produces(MediaType.APPLICATION_JSON)
	public DTOInterface getAllData()

	{
		//Tell logger what's happening
		logger.info("Enter getAllData method");
		//Create list of weather
		List<Weather> weather = new ArrayList<Weather>();
		try {
			//Get all weather from dao
			weather = dao.findAll();
			//New factory class member variable
			DTOFactoryClass dtoFactory = new DTOFactoryClass();
			//Creating the response
			DTOInterface response = dtoFactory.DTOFactory(0, "", weather);
			logger.info("Got data from getAllData()");
			//Returning the response
			return response;

		} catch (Exception e2) {
			//Catches exceptions and responds with system exception error
			DTOFactoryClass dtoFactory = new DTOFactoryClass();
			DTOInterface response = dtoFactory.DTOFactory(-2, "System Exception", weather);
			logger.error("Error in getAllData");
			return response;
		}
	}

}
