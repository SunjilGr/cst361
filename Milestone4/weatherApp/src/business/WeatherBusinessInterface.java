package business;

import java.util.List;

import javax.ejb.Local;

import beans.Weather;

/**
 * The Interface WeatherBusinessInterface.
 *
 * @author Aaron and Sunjil
 * 
 *         Interface for weather business service
 */
@Local
public interface WeatherBusinessInterface {
	
	/**
	 * Insert weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Method for inserting weather data
	 * @param weather the weather
	 */
	public void insertWeatherData(Weather weather);
	
	/**
	 * Gets the weather data.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Method for getting weather data
	 * @return the weather data
	 */
	public List<Weather> getWeatherData();

}
