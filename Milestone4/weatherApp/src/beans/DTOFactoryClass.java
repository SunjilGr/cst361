package beans;

import java.util.List;

/**
 * DTO factory class to return a Data model with status, message, and data. 
 *   This fulfills the factory method required for this milestone.
 * @author Aaron and Sunjil
 *
 */
public class DTOFactoryClass {
	
	/**
	 * DTO factory.
	 *
	 * @param status the status
	 * @param message the message
	 * @param data the data
	 * @return the DTO interface
	 */
	public DTOInterface DTOFactory(int status, String message, List<Weather> data)
	{
		
		return new ResponseDataModel(status, message,data);
	}

}
