package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * User class with the user variables.
 * 
 * @author Sunjil and Aaron
 *
 */
@ViewScoped
@ManagedBean
public class User {

	/** The first name. */
	@NotNull(message = "First name is a required field")
	@Size(min = 5, max = 20, message = "First Name must be 5 to 20 character  long")
	public String firstName = "";

	/** The last name. */
	@NotNull(message = "Last name is a required field")
	@Size(min = 5, max = 20, message = "Last name must be 5 to 20 character  long")
	public String lastName = "";

	/** The username. */
	@NotNull(message = "Username is a required field")
	@Size(min = 5, max = 20, message = "Username must be 5 to 20 character  long")
	public String username = "";

	/** The password. */
	@NotNull(message = "Password is a required field")
	@Size(min = 5, max = 20, message = "Password must be 5 to 20 character long")
	public String password = "";

	/**
	 * Instantiates a new user.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Default Constructor
	 */
	public User() {
		firstName = "";
		lastName = "";
		username = "";
		password = "";
	}

	/**
	 * Gets the first name.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the username.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @author Aaron and Sunjil
	 * 
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
