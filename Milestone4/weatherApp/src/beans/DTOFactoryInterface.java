package beans;

import java.util.List;

/**
 * DTO factory interface class using generics. 
 *   This fulfills the factory method required for this milestone.
 *
 * @author Aaron and Sunjil
 * @param <T> the generic type
 */
public interface DTOFactoryInterface<T> {
	//public DTOFactoryClass DTOFactory(int t, string t, List<T> t);

}
