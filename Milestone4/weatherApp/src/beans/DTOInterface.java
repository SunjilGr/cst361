package beans;

import java.util.List;

/**
 * This is the DTO interface class creating a "contract" by which dto objects must fulfill.
 * @author Aaron and Sunjil
 *
 */
public interface DTOInterface {

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public List<Weather> getData();
	
	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(List<Weather> data);
}
