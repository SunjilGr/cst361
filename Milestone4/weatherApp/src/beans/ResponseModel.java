package beans;

/**
 * Response model class with status and message variables.
 *
 * @author Aaron and Sunjil
 */
public class ResponseModel {
	
	/** The status. */
	int status;
	
	/** The message. */
	String message;
	
	/**
	 * Non-default constructor for response model initializing status and message.
	 *
	 * @author Aaron and Sunjil
	 * @param status the status
	 * @param message the message
	 */
	public ResponseModel(int status, String message) {
		
		this.status = status;
		this.message = message;
	}
	
	/**
	 * Get status method.
	 *
	 * @author Aaron and Sunjil
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * Set status method.
	 *
	 * @author Aaron and Sunjil
	 * @param status the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * Get message method.
	 *
	 * @author Aaron and Sunjil
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Set message method.
	 *
	 * @author Aaron and Sunjil
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

}
