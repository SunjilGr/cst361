package beans;

/**
 * The Class Weather.
 *
 * @author Aaron and Sunjil
 * 
 *         Weather class for holding all weather data
 */
public class Weather {
	
	/** The temperature. */
	//Creating all variables
	double temperature;
	
	/** The max temp. */
	double maxTemp;
	
	/** The min temp. */
	double minTemp;
	
	/** The humidity. */
	double humidity;
	
	/** The date and time. */
	String dateAndTime;
	
	/** The wid. */
	int WID;
	
	/**
	 * Instantiates a new weather.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Default Constructor
	 */
	public Weather()
	{
		temperature=0;
		maxTemp=0;
		minTemp=0;
		humidity=0;
	}
	
	/**
	 * Instantiates a new weather.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Non-Default Constructor
	 * @param WID the wid
	 * @param temperature the temperature
	 * @param maxTemp the max temp
	 * @param minTemp the min temp
	 * @param humidity the humidity
	 * @param dateAndTime the date and time
	 */
	public Weather(int WID, double temperature, double maxTemp, double minTemp, double humidity, String dateAndTime) {
		this.WID= WID;
		this.temperature = temperature;
		this.maxTemp = maxTemp;
		this.minTemp = minTemp;
		this.humidity = humidity;
		this.dateAndTime= dateAndTime;
	}
	
	/**
	 * Gets the temperature.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for temperature
	 * @return the temperature
	 */
	public double getTemperature() {
		return temperature;
	}
	
	/**
	 * Sets the temperature.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for temperature
	 * @param temperature the new temperature
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	/**
	 * Gets the max temp.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for max temp
	 * @return the max temp
	 */
	public double getMaxTemp() {
		return maxTemp;
	}
	
	/**
	 * Sets the max temp.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for max temp
	 * @param maxTemp the new max temp
	 */
	public void setMaxTemp(double maxTemp) {
		this.maxTemp = maxTemp;
	}
	
	/**
	 * Gets the min temp.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for min temp
	 * @return the min temp
	 */
	public double getMinTemp() {
		return minTemp;
	}
	
	/**
	 * Sets the min temp.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for min temp
	 * @param minTemp the new min temp
	 */
	public void setMinTemp(double minTemp) {
		this.minTemp = minTemp;
	}
	
	/**
	 * Gets the humidity.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for humidity
	 * @return the humidity
	 */
	public double getHumidity() {
		return humidity;
	}
	
	/**
	 * Sets the humidity.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for humidity
	 * @param humidity the new humidity
	 */
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	
	/**
	 * Gets the date and time.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for date and time
	 * @return the date and time
	 */
	public String getDateAndTime() {
		return dateAndTime;
	}
	
	/**
	 * Sets the date and time.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for date and time
	 * @param dateAndTime the new date and time
	 */
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	
	/**
	 * Gets the wid.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for weather id
	 * @return the wid
	 */
	public int getWID() {
		return WID;
	}
	
	/**
	 * Sets the wid.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for weather id
	 * @param wID the new wid
	 */
	public void setWID(int wID) {
		WID = wID;
	}
	

}
