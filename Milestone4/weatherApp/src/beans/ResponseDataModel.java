package beans;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Response data model class extending response model and implementing the DTO interface.
 * @author Aaron and Sunjil
 *
 */
@XmlRootElement(name="Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDataModel extends ResponseModel implements DTOInterface{
	
	/** The data. */
	private List<Weather> data;
	
	/**
	 * Default Constructor for the response data model.
	 * @author Aaron and Sunjil
	 *
	 */
	public ResponseDataModel()
	{
		super(0,"");
		this.data=null;
	}

	/**
	 * Non-default constructor for response data model that takes an int, string and list of weather data.
	 *
	 * @author Aaron and Sunjil
	 * @param status the status
	 * @param message the message
	 * @param data the data
	 */
	 public ResponseDataModel(int status, String message, List<Weather> data)
	 {
		 super(status,message);
		 this.data=data;
	 }
	 
	 /**
 	 * Get method for the weather data that returns list of weather data.
 	 *
 	 * @author Aaron and Sunjil
 	 * @return the data
 	 */
	public List<Weather> getData() {
		return data;
	}

	/**
	 * Setter method for data. This sets the list of weather data.
	 *
	 * @author Aaron and Sunjil
	 * @param data the new data
	 */
	public void setData(List<Weather> data) {
		this.data = data;
	}
	
	

}
