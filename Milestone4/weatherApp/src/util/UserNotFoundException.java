package util;

/**
 * User not found exception class extending exception. This is checked.
 * @author awros
 *
 */
public class UserNotFoundException extends Exception{
	
	/** The Constant serialVersionID. */
	private static final long serialVersionID=1111;

}
