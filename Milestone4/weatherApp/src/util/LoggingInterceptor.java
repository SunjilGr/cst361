package util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LoggingInterceptor.
 *
 * @author Aaron and Sunjil
 * 
 *         Logging intercepter for the application wide logger
 */
public class LoggingInterceptor {

	/** The logger. */
	//Creating the logger from the logger factory of slf4j
	Logger logger= LoggerFactory.getLogger(LoggingInterceptor.class);
	
	/**
	 * Method interceptor.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Method intercepter for intercepting calls to and from methods and classes
	 * @param ctx the ctx
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object methodInterceptor(InvocationContext ctx) throws Exception
	{
		logger.info("*********** Intercepting call to method: "+ctx.getTarget().getClass()+"_"+ctx.getMethod().getName()+"()");
		return ctx.proceed();
	}
}
