package util;

/**
 * Database exception class extends runtime and initializes a serialVersionID.
 *
 * @author awros
 */
public class DatabaseException extends RuntimeException{
	
	/** The Constant serialVersionID. */
	private static final long serialVersionID=111111;
	
	/**
	 * Database exception method takes in a string.
	 *
	 * @param a the a
	 */
	public DatabaseException(Throwable a)
	{
		super(a);
	}


}
