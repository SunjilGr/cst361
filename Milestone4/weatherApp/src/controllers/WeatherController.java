package controllers;

import java.io.IOException;



import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import beans.Weather;
import business.WeatherBusinessInterface;
import util.LoggingInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WeatherController.
 *
 * @author Aaron and Sunjil
 * 
 *         Weather controller that injects the weather business interface
 */
@ManagedBean
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class WeatherController {

	/** The interf. */
	@Inject
	WeatherBusinessInterface interf;
	
	/** The logger. */
	Logger logger= LoggerFactory.getLogger(WeatherController.class);
	
	/**
	 * On submit view table.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Viewing the table when the submit is clicked
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String onSubmitViewTable() throws IOException
	{
		logger.info("Entering onSubmitViewTable in WeatherController");
		FacesContext context = FacesContext.getCurrentInstance();
		Weather weather = context.getApplication().evaluateExpressionGet(context, "#{weather}", Weather.class);
		// forward to HomePage form along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("weather", weather);
		System.out.println("hello");
		if(interf.getWeatherData()!=null)
		{
			logger.info("Found data to show in data table");
			logger.info("Existing onSubmitViewTable in WeatherController");
				return "DataTable";
		}
		else
			logger.info("No data to show in data table");
		    logger.info("Existing onSubmitViewTable in WeatherController");
			return"HomePage.xhtml";
	}
	
	/**
	 * On submit view chart.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Viewing the chart when submit is clicked
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String onSubmitViewChart() throws IOException
	{
		logger.info("Entering onSubmitViewChart in WeatherController");
		FacesContext context = FacesContext.getCurrentInstance();
		Weather weather = context.getApplication().evaluateExpressionGet(context, "#{weather}", Weather.class);
		// forward to HomePage form along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("weather", weather);
		System.out.println("reached controller method for chart");
		logger.info("Existing onSubmitViewChart in WeatherController");
				return "DataChart.xhtml";
	}
	
	/**
	 * Gets the interface.
	 *
	 * @author Aaron and Sunjil
	 * 
	 *         Getting the weather business interface and returning the interface
	 * @return the interface
	 */
	public WeatherBusinessInterface getInterface() {
		return interf;
	}
	



}
