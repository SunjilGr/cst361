/**
 * Class: UserController
 * Objective: Direct request to correct pages and methods
 * Detail: This class contains two method onSubmitRegsitration and OnSumbit login which is used to register and login user respectively and direct them to correct pages
 * Date: 9/30/2018
 * Reference: CST-235
 */
package controllers;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;
import business.UserBusinessInterface;
import business.WeatherBusinessInterface;
import util.LoggingInterceptor;
import util.UserNotFoundException;

/**
 * User controller class for directing user to register, weather display, and login. Uses an injected logger.
 * @author Aaron and Sunjil
 *
 */
@ManagedBean
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class UserController {
	
	/** The interf. */
	@Inject
	UserBusinessInterface interf;
	
	/** The service. */
	@Inject
	WeatherBusinessInterface service;
	
	 /** The logger. */
 	Logger logger= LoggerFactory.getLogger(UserController.class);
	
	/**
	 * This method is called when users submit their information on RegistrationForm.xhtml. 
	 * It directs users to the LoginForm if the all the required fields are filled out.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String onSubmitRegistration() throws IOException
	{
		// get the User Managed Bean
				FacesContext context = FacesContext.getCurrentInstance();
				User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

				// forward to Login form along with the User Managed Bean
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				//Putting user filled out information from the form to the database through bussiness interface
				interf.register(user);
				logger.info("Registration success");
				return "LoginForm.xhtml";
	}
	
	/**
	 * This method is called when user submit login inforamtion using LoginForm.xhtml
	 * It directs user to the HomePage if the username and password matches with information on database
	 *
	 * @return the string
	 */
	public String onSubmitLogin()  {
		// get the User Managed Bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		// forward to HomePage form along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		
		try{
			interf.login(user);
		}
		catch(UserNotFoundException e)
		{
			String info="Could not find user";
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("info", info);
			return "LoginForm.xhtml";

		}
		return "HomePage.xhtml";
	}
	
	
	

	/**
	 * Method to get the user business interface.
	 *
	 * @author Aaron and Sunjil
	 * @return the interface
	 */
	public UserBusinessInterface getInterface() {
		return interf;
	}
	
	/**
	 * Method to get the weather business interface.
	 *
	 * @author Aaron and Sunjil
	 * @return the weather interface
	 */
	public WeatherBusinessInterface getWeatherInterface()
	{
		return service;
	}

}
