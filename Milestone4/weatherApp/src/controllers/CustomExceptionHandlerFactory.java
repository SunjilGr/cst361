package controllers;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * A factory for creating CustomExceptionHandler objects.
 */
public class CustomExceptionHandlerFactory extends ExceptionHandlerFactory
{
	 
 	/** The parent. */
 	private ExceptionHandlerFactory parent;
	 
	 /**
 	 * Instantiates a new custom exception handler factory.
 	 *
 	 * @param parent the parent
 	 */
 	public CustomExceptionHandlerFactory(ExceptionHandlerFactory parent) 
	 {
		 this.parent = parent;
	 }

	/* (non-Javadoc)
	 * @see javax.faces.context.ExceptionHandlerFactory#getExceptionHandler()
	 */
	@Override
	public ExceptionHandler getExceptionHandler()
	{
		return new CustomExceptionHandler(this.parent.getExceptionHandler());
	}
}
