package controllers;

import java.util.Iterator;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

/**
 * The Class CustomExceptionHandler.
 *
 * @author awros
 * Custom exception handler extending exception handler wrapper
 */
public class CustomExceptionHandler extends ExceptionHandlerWrapper
{
	
	/** The wrapped. */
	private ExceptionHandler wrapped;

	/**
	 * Default constructor.
	 *
	 * @param exception the exception
	 */
	CustomExceptionHandler(ExceptionHandler exception)
	{
		this.wrapped = exception;
	}

	/* (non-Javadoc)
	 * @see javax.faces.context.ExceptionHandlerWrapper#getWrapped()
	 */
	@Override
	public ExceptionHandler getWrapped()
	{
		return wrapped;
	}

	/* (non-Javadoc)
	 * @see javax.faces.context.ExceptionHandlerWrapper#handle()
	 */
	@Override
    public void handle() throws FacesException
    {
        Iterator<ExceptionQueuedEvent> queue = getUnhandledExceptionQueuedEvents().iterator();

        while (queue.hasNext())
        {
            ExceptionQueuedEvent item = queue.next();
            ExceptionQueuedEventContext exceptionQueuedEventContext = (ExceptionQueuedEventContext)item.getSource();
            try 
            {
                Throwable throwable = exceptionQueuedEventContext.getException();
                
                //StringWriter sw = new StringWriter();
                //PrintWriter pw = new PrintWriter(sw);
              //  throwable.printStackTrace(pw);

                FacesContext context = FacesContext.getCurrentInstance();
                Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
                NavigationHandler nav = context.getApplication().getNavigationHandler();
                requestMap.put("error-message", throwable.getMessage());
                //requestMap.put("error-stack", sw.toString());
                nav.handleNavigation(context, null, "Error.xhtml");
                context.renderResponse();
            } 
            finally 
            {
                queue.remove();
            }
        }
	}
}