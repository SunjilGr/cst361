/**
 * Class: UserDataInterface
 * Objective: This is the data interface class. This is used to access methods inside DAO class. Inversion of control (IOC) is made possible by this class
 * Detail: It contains two method registerUser and login. Both of them are override in DAO class
 * Date: 9/30/2018
 */
package data;
import javax.ejb.Local;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *        Interface for the user data
 */
@Local
public interface UserDataInterface <T>{
	
	/**
	 * Insert user's information into database to register them
	 * @param fName
	 * @param lName
	 * @param uName
	 * @param pass
	 */
	public boolean registerUser(T t);
	
	/**
	 * Check username and password to login. Return true if he information matches and false if they don't
	 * @param uName
	 * @param pass
	 * @return
	 */
	public boolean login(T t);

	

}
