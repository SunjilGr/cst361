package data;

import java.util.List;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Interface for weather data
 */
public interface WeatherDataInterface <T> {
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         create method for weather data
	 */
	public boolean createWeatherData(T t);

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Find all method
	 */
	public List<T> findAll();

}
