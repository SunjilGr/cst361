package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Weather;
import util.LoggingInterceptor;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Weather database access object implements weather data interface
 */
@Stateless
@Local(WeatherDataInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class WeatherDAO implements WeatherDataInterface<Weather> {
	
	//New weather model of data and boolean result
	Weather data= new Weather();
	boolean result;

	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Create method for weather data
	 */
	@Override
	public boolean createWeatherData(Weather weather)
	{
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
		   LocalDateTime now = LocalDateTime.now();
		   
			//connection string
			Connection conn = null;
			String url = "jdbc:derby:C:\\\\Users\\\\sunze\\\\WeatherDB";
			String username = "user";
			String password = "derby";
			PreparedStatement sql = null;
			
			//try block that contains insert query
			try {
				// use prepare statement to execute sql
				conn = DriverManager.getConnection(url, username, password);
				sql = conn.prepareStatement(
						"INSERT INTO WeatherDB.WEATHER(TEMP, TEMPMIN, TEMPMAX, HUMIDITY,DATEANDTIME) VALUES (?,?,?,?,?)");
				sql.setDouble(1, weather.getTemperature());
				sql.setDouble(2, weather.getMinTemp());
				sql.setDouble(3, weather.getMaxTemp());
				sql.setDouble(4, weather.getHumidity());
				//sql.setString(5,"1");
				sql.setString(5, dtf.format(now));
				
				sql.executeUpdate();
				
			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("SQL statement error");
				e.printStackTrace();
			}
			return result;
					
	}
	
	@Override
	/**
	 * SELECT all products from products table
	 */
	public List<Weather> findAll() {
		//connection string
		Connection conn = null;
		String url = "jdbc:derby:C:\\Users\\sunze\\WeatherDB";
		String username = "user";
		String password = "derby";
		//PreparedStatement sql1 = null;
		// sql statement
		 String sql1 = "SELECT * FROM WeatherDB.WEATHER";
		List<Weather> weatherList = new ArrayList<Weather>();

		try {
			// connection string
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql1);
			// set data to corresponding attribute
			while (rs.next()) {
				
				data.setWID(rs.getInt("WID"));
				data.setTemperature(rs.getDouble("TEMP"));
				data.setMinTemp(rs.getDouble("TEMPMIN"));
				data.setMaxTemp(rs.getDouble("TEMPMAX"));
				data.setHumidity(rs.getDouble("HUMIDITY"));
				data.setDateAndTime(rs.getString("DATEANDTIME"));
			   
				weatherList.add(new Weather(rs.getInt("WID"), rs.getDouble("TEMP"), rs.getDouble("TEMPMIN"), rs.getDouble("TEMPMAX"), rs.getDouble("HUMIDITY"), rs.getString("DATEANDTIME")));
			}
			// close result set
			rs.close();
		}
		// exception catching
		catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		// return products list
		return weatherList;
	}

	

	
	

}
