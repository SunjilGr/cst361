package business;

import java.util.List;

import javax.ejb.Local;

import beans.Weather;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Interface for weather business service
 */
@Local
public interface WeatherBusinessInterface {
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Method for inserting weather data
	 */
	public void insertWeatherData(Weather weather);
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Method for getting weather data
	 */
	public List<Weather> getWeatherData();

}
