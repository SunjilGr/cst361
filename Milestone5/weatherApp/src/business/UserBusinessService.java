package business;

import javax.ejb.Local;
import javax.ejb.Stateless;

import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;

import data.UserDataInterface;
import util.LoggingInterceptor;
import javax.enterprise.inject.Alternative;

/**
 * 
 * @author Aaron and Sunjil
 * 
 * Class: UserBusinessService
 * Objective: Contains business logic required to login and register user
 * Detail: It contains two methods, register and login which is used to register and login users respectively.
 * Date: 9/30/2018
 * 
 */

@Stateless
@Local(UserBusinessInterface.class)
@Interceptors(LoggingInterceptor.class)
public class UserBusinessService implements UserBusinessInterface {

@Inject	
UserDataInterface<User> service;
			
Logger logger= LoggerFactory.getLogger(UserBusinessService.class);
	/**
	 * This method contains business logic to register user. 
	 * It takes firstname, lastname, username, and password as parameter 
	 * It is a method with void return type therefore it does not return anything
	 */
	
		public boolean register(User user) {
			logger.info("Entering register method in UserBusinessService class");
		if(service.registerUser(user))
		{
			return true;
		}
		
		else
			logger.info("Existing register method in UserBusinessService class");
			return false;

		}

	
	/**
	 * This method contains bussiness logic to login user.
	 * It takes username and password as parameter and return true or false based on the result provided by DAO class
	 */
	public boolean login(User user){
		logger.info("Entering login method in UserBusinessService class");
		if (service.login(user))
		{
			return true;
		}
		
		else
			logger.info("Existing login method in UserBusinessService class");
			return false;

	}

}
