package business;



import javax.ejb.Local;

import beans.User;


/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Interface for the user business service
 */
@Local
public interface UserBusinessInterface {

	/**
	 *  Insert user's information into database to register them
	 * @param uName
	 * @param pass
	 * @return
	 */
	public boolean login(User user);
	
	/**
	 * Check username and password to login. Return true if the information matches and false if they don't
	 * @param fName
	 * @param lName
	 * @param uName
	 * @param pass
	 */
	public boolean register(User user);
	
}
