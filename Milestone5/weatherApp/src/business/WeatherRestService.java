package business;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.DTOFactoryClass;
import beans.DTOInterface;

import beans.Weather;

import data.WeatherDataInterface;
import util.LoggingInterceptor;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Weather rest service class that injects the weather data interface
 *         (DAO) and initializes the logger
 */
@RequestScoped
@Path("/weather")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
@Interceptors(LoggingInterceptor.class)
public class WeatherRestService {

	@Inject
	WeatherDataInterface<Weather> dao;

	Logger logger = LoggerFactory.getLogger(WeatherRestService.class);

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         method for saving the temperature and returning a response
	 */
	@POST
	@Path("/save")
	@Consumes("application/json")
	@Produces("application/json")
	public Response saveTemperature(Weather weather) {
		// create the weather data
		dao.createWeatherData(weather);
		// create the result and print it to the console to ensure product creation
		String result = "Product created : " + weather;
		System.out.println(result);
		// return the response
		return Response.status(201).entity(result).build();
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getting all data for charts. Takes the DTO interface
	 */
	@GET
	@Path("/getChartData")
	@Produces(MediaType.APPLICATION_JSON)
	public DTOInterface getAllData()

	{
		//Tell logger what's happening
		logger.info("Enter getAllData method");
		//Create list of weather
		List<Weather> weather = new ArrayList<Weather>();
		try {
			//Get all weather from dao
			weather = dao.findAll();
			//New factory class member variable
			DTOFactoryClass dtoFactory = new DTOFactoryClass();
			//Creating the response
			DTOInterface response = dtoFactory.DTOFactory(0, "", weather);
			logger.info("Got data from getAllData()");
			//Returning the response
			return response;

		} catch (Exception e2) {
			//Catches exceptions and responds with system exception error
			DTOFactoryClass dtoFactory = new DTOFactoryClass();
			DTOInterface response = dtoFactory.DTOFactory(-2, "System Exception", weather);
			logger.error("Error in getAllData");
			return response;
		}
	}

}
