package util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import controllers.UserController;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Logging intercepter for the application wide logger
 */
public class LoggingInterceptor {

	//Creating the logger from the logger factory of slf4j
	Logger logger= LoggerFactory.getLogger(LoggingInterceptor.class);
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Method intercepter for intercepting calls to and from methods and classes
	 */
	@AroundInvoke
	public Object methodInterceptor(InvocationContext ctx) throws Exception
	{
		logger.info("*********** Intercepting call to method: "+ctx.getTarget().getClass()+"_"+ctx.getMethod().getName()+"()");
		return ctx.proceed();
	}
}
