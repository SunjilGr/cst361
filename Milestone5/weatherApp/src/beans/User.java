package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ViewScoped
@ManagedBean

/**
 * User class with the user variables.
 * 
 * @author Sunjil and Aaron
 *
 */
public class User {

	/*
	 * 
	 * @author Aaron and Sunjil
	 *
	 * Variables with Data validation constraints
	 */
	@NotNull(message = "First name is a required field")
	@Size(min = 5, max = 20, message = "First Name must be 5 to 20 character  long")
	public String firstName = "";

	@NotNull(message = "Last name is a required field")
	@Size(min = 5, max = 20, message = "Last name must be 5 to 20 character  long")
	public String lastName = "";

	@NotNull(message = "Username is a required field")
	@Size(min = 5, max = 20, message = "Username must be 5 to 20 character  long")
	public String username = "";

	@NotNull(message = "Password is a required field")
	@Size(min = 5, max = 20, message = "Password must be 5 to 20 character long")
	public String password = "";

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Default Constructor
	 */
	public User() {
		firstName = "";
		lastName = "";
		username = "";
		password = "";
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
