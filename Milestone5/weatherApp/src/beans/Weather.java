package beans;

/**
 * 
 * @author Aaron and Sunjil
 * 
 *         Weather class for holding all weather data
 */
public class Weather {
	
	//Creating all variables
	double temperature;
	double maxTemp;
	double minTemp;
	double humidity;
	String dateAndTime;
	int WID;
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Default Constructor
	 */
	public Weather()
	{
		temperature=0;
		maxTemp=0;
		minTemp=0;
		humidity=0;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Non-Default Constructor
	 */
	public Weather(int WID, double temperature, double maxTemp, double minTemp, double humidity, String dateAndTime) {
		this.WID= WID;
		this.temperature = temperature;
		this.maxTemp = maxTemp;
		this.minTemp = minTemp;
		this.humidity = humidity;
		this.dateAndTime= dateAndTime;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for temperature
	 */
	public double getTemperature() {
		return temperature;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for temperature
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for max temp
	 */
	public double getMaxTemp() {
		return maxTemp;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for max temp
	 */
	public void setMaxTemp(double maxTemp) {
		this.maxTemp = maxTemp;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for min temp
	 */
	public double getMinTemp() {
		return minTemp;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for min temp
	 */
	public void setMinTemp(double minTemp) {
		this.minTemp = minTemp;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for humidity
	 */
	public double getHumidity() {
		return humidity;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for humidity
	 */
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for date and time
	 */
	public String getDateAndTime() {
		return dateAndTime;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for date and time
	 */
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Getter for weather id
	 */
	public int getWID() {
		return WID;
	}
	
	/**
	 * 
	 * @author Aaron and Sunjil
	 * 
	 *         Setter for weather id
	 */
	public void setWID(int wID) {
		WID = wID;
	}
	

}
